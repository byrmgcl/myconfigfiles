#!/bin/bash

event_type=EV_KEY
action_type=POINTER_BUTTON
pressed="pressed,"

readarray -t devices <<<$(libinput list-devices | grep pointer -B3 | grep -o '/dev/input/event[1-9]*')

# COMMANDS MAP
BTN_EXTRA=(0)
BTN_SIDE=(1)

function pressKey(){
    device=$1; key=$2; value=$3
    echo "pressing ${key} ${value}"
    number=`wmctrl -d | grep \* | awk '{print $1}'`
    if [ ${key} = 0 ]; then
        if [ $value -eq 1 ]; then
            if [ $number -ne 0 ]; then
                wmctrl -s $(($number-1))
            fi
        fi
    elif [ ${key} = 1 ]; then
        if [ $value -eq 1 ]; then
            wmctrl -s $(($number+1))
        fi
    fi
    evemu-event /dev/input/${device} --sync --type ${event_type} --code ${key} --value ${value};
}

function pressCommand(){
    device=$1; button=$2; movement=$3
    var=$button[@]
    command=${!var}

    if [ ${movement} = ${pressed} ]; then
        for key in ${command}; do
            pressKey ${device} ${key} 1
        done
    else
        for key in ${command}; do
            pressKey ${device} ${key} 0
        done | tac
    fi
}

function parseEventLine(){
    device=$1
    action=$2
    button=$4
    movement=$6

    # compute only if right action
    if [ ${action} = ${action_type} ]; then
        pressCommand ${device} ${button} ${movement}
    fi
}

function mapDevice(){
    device=$1
    while read line; do
        parseEventLine ${line}
    done < <(stdbuf -oL libinput debug-events --device ${device} & )
}

for device in ${devices[@]}; do
    ( mapDevice ${device} ) &
done

wait
