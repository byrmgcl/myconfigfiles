(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   ["#2f2f2e" "#ffb4ac" "#8ac6f2" "#e5c06d" "#a4b5e6" "#e5786d" "#7ec98f" "#74736f"])
 '(ansi-term-color-vector
   [unspecified "#2d2a2e" "#ff6188" "#a9dc76" "#ffd866" "#78dce8" "#ab9df2" "#a1efe4" "#fcfcfa"])
 '(awesome-tray-mode-line-active-color "#2fafff")
 '(awesome-tray-mode-line-inactive-color "#323232")
 '(ccm-recenter-at-end-of-file t)
 '(ccm-vpos-init '(round (ccm-visible-text-lines) 2))
 '(centaur-tabs-hide-tabs-hooks '(reb-mode-hook completion-list-mode-hook))
 '(column-number-mode t)
 '(company-idle-delay 0.7)
 '(company-tooltip-idle-delay 0.5)
 '(compilation-message-face 'default)
 '(css-indent-offset 2)
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-enabled-themes '(tangotango))
 '(custom-safe-themes
   '("6ca5f925de5c119694dbe47e2bc95f8bad16b46d154b3e2e0ae246fec4100ec5" "e76668f7efa2c9297c3998aba61876e757c99c1e5325c135433cf4f53a95df15" "c5a81a42df109b02a9a68dfe0ed530080372c1a0bbcb374da77ee3a57e1be719" "f149d9986497e8877e0bd1981d1bef8c8a6d35be7d82cba193ad7e46f0989f6a" "ed51f8c517af6b8c05c055b52913d0da3a2b6f42ec5f6b8c7c79f79c83158403" "c8b83e7692e77f3e2e46c08177b673da6e41b307805cd1982da9e2ea2e90e6d7" "90a6f96a4665a6a56e36dec873a15cbedf761c51ec08dd993d6604e32dd45940" "409e4d689f1e29e5a18f536507e6dc760ee9da76dc56481aaa0696705e6be968" "be7177b0a01396e6f0c7da06c5d295bafee7d51363d9d6f840efc1e15fa8ff35" "7c20c453ad5413b110ccc3bb5df07d69999d741d29b1f894bd691f52b4abdd31" "eb122e1df607ee9364c2dfb118ae4715a49f1a9e070b9d2eb033f1cefd50a908" "4b62f25863fb026a14c1d7683dc83da4f0af07fe0cacd1b52f0e05639d483e83" "2050674326d536ddd3dcea87e077d27071cfbbe974a4540b1a57b6b672f64c51" "171d1ae90e46978eb9c342be6658d937a83aaa45997b1d7af7657546cae5985b" "b494aae329f000b68aa16737ca1de482e239d44da9486e8d45800fd6fd636780" "70f5a47eb08fe7a4ccb88e2550d377ce085fedce81cf30c56e3077f95a2909f2" "5a0eee1070a4fc64268f008a4c7abfda32d912118e080e18c3c865ef864d1bea" "7397cc72938446348521d8061d3f2e288165f65a2dbb6366bb666224de2629bb" "9f1d0627e756e58e0263fe3f00b16d8f7b2aca0882faacdc20ddd56a95acb7c2" "9f218080c0526490543e9ce0dc539c092b1777872930d96a0585cf11caaff842" "c98a51cb51a82810f98bcec972dd07971c4e3447fd44e866167f1cb245ab1ddc" "03336a06e95a977ce3d67f849b93d492736d6bd11d61dddc424c81d7819c8ad1" "a603ec90259dd38c3e5ecc74a9cf51c7ad356f1d0d645e5f678cf814c4ed39b5" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "ed68393e901a88b9feefea1abfa9a9c5983e166e4378c71bb92e636423bd94fd" "78e6be576f4a526d212d5f9a8798e5706990216e9be10174e3f3b015b8662e27" "1d89fcf0105dd8778e007239c481643cc5a695f2a029c9f30bd62c9d5df6418d" "88deeaaab5a631834e6f83e8359b571cfcbf5a18b04990288c8569cc16ee0798" "5e2cdea6453f8963037723ab91c779b203fb201bf5c377094440f0c465d688ec" "51ec7bfa54adf5fff5d466248ea6431097f5a18224788d0bd7eb1257a4f7b773" "efcecf09905ff85a7c80025551c657299a4d18c5fcfedd3b2f2b6287e4edd659" "57a29645c35ae5ce1660d5987d3da5869b048477a7801ce7ab57bfb25ce12d3e" "3e200d49451ec4b8baa068c989e7fba2a97646091fd555eca0ee5a1386d56077" "833ddce3314a4e28411edf3c6efde468f6f2616fc31e17a62587d6a9255f4633" "d89e15a34261019eec9072575d8a924185c27d3da64899905f8548cbd9491a36" "4c56af497ddf0e30f65a7232a8ee21b3d62a8c332c6b268c81e9ea99b11da0d3" "830877f4aab227556548dc0a28bf395d0abe0e3a0ab95455731c9ea5ab5fe4e1" "7f1d414afda803f3244c6fb4c2c64bea44dac040ed3731ec9d75275b9e831fe5" "fee7287586b17efbfda432f05539b58e86e059e78006ce9237b8732fde991b4c" "27a1dd6378f3782a593cc83e108a35c2b93e5ecc3bd9057313e1d88462701fcd" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "e208e45345b91e391fa66ce028e2b30a6aa82a37da8aa988c3f3c011a15baa22" "0feb7052df6cfc1733c1087d3876c26c66410e5f1337b039be44cb406b6187c6" "3632cf223c62cb7da121be0ed641a2243f7ec0130178722554e613c9ab3131de" "66132890ee1f884b4f8e901f0c61c5ed078809626a547dbefbb201f900d03fd8" "2df493c5c7f329eef362290abdcd42a45abad98ffe33f639ecc55af084224e8b" "fc6697788f00629cd01f4d2cc23f1994d08edb3535e4c0facef6b7247b41f5c7" "c221703cc604312f6f72349704f7329f80ccc6a261af769332ec80171b728cc0" default))
 '(dap-auto-configure-features '(sessions locals breakpoints tooltip))
 '(dap-debug-restart-keep-session nil)
 '(dap-label-output-buffer-category t)
 '(dap-output-window-max-height 44)
 '(dap-output-window-min-height 20)
 '(deft-archive-directory "arşiv/")
 '(deft-directory "/bayram/Yandex.Disk/zetteldeft")
 '(deft-extensions '("org" "md" "markdown" "txt" "text"))
 '(deft-file-limit 68)
 '(deft-recursive t)
 '(deft-strip-summary-regexp
   "\\([
	]\\|^#.+:.*$\\|\\[\\[zdlink:.+\\[\\|\\[\\[\\|\\]\\]\\|[[:space:]]+:PROPERTIES:
[[:space:]]+:CREATED:.*
[[:space:]]+:END:\\|--+\\)")
 '(display-fill-column-indicator-column t)
 '(display-line-numbers-type nil)
 '(dumb-jump-selector 'ivy)
 '(fci-rule-color "#2f2f2e" t)
 '(fill-column 79)
 '(flycheck-display-errors-function 'ignore)
 '(flycheck-idle-change-delay 0.3)
 '(flycheck-phpcs-standard "PSR2")
 '(flymake-error-bitmap '(flymake-double-exclamation-mark modus-theme-fringe-red))
 '(flymake-note-bitmap '(exclamation-mark modus-theme-fringe-cyan))
 '(flymake-warning-bitmap '(exclamation-mark modus-theme-fringe-yellow))
 '(font-use-system-font t)
 '(gkroam-mode t)
 '(gkroam-show-brackets-flag t)
 '(gkroam-use-default-filename t)
 '(gkroam-window-margin 4)
 '(global-font-lock-mode t)
 '(global-hi-lock-mode nil)
 '(global-linum-mode nil)
 '(highlight-changes-colors '("#d33682" "#6c71c46c71c4"))
 '(highlight-symbol-colors
   '("#98695021d64f" "#484f5a50ffff" "#9ae80000c352" "#00000000ffff" "#98695021d64f" "#9ae80000c352" "#484f5a50ffff"))
 '(highlight-symbol-foreground-color "#93a1a1")
 '(highlight-tail-colors
   '(("#073642" . 0)
     ("#5b7300" . 20)
     ("#007d76" . 30)
     ("#0061a8" . 50)
     ("#866300" . 60)
     ("#992700" . 70)
     ("#a00559" . 85)
     ("#073642" . 100)))
 '(hl-bg-colors
   '("#866300" "#992700" "#a7020a" "#a00559" "#243e9b" "#0061a8" "#007d76" "#5b7300"))
 '(hl-fg-colors
   '("#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36"))
 '(hl-paren-colors '("#2aa198" "#b58900" "#268bd2" "#6c71c4" "#859900"))
 '(hl-sexp-background-color "#efebe9")
 '(hl-todo-keyword-faces
   '(("TODO" . "#dc752f")
     ("NEXT" . "#dc752f")
     ("THEM" . "#2d9574")
     ("PROG" . "#3a81c3")
     ("OKAY" . "#3a81c3")
     ("DONT" . "#f2241f")
     ("FAIL" . "#f2241f")
     ("DONE" . "#42ae2c")
     ("NOTE" . "#b1951d")
     ("KLUDGE" . "#b1951d")
     ("HACK" . "#b1951d")
     ("TEMP" . "#b1951d")
     ("INSPECT" . "#dc752f")
     ("FIXME" . "#dc752f")
     ("XXX" . "#dc752f")
     ("XXXX" . "#dc752f")))
 '(ibuffer-deletion-face 'diredp-deletion-file-name)
 '(ibuffer-filter-group-name-face 'modus-theme-mark-symbol)
 '(ibuffer-marked-face 'diredp-flag-mark)
 '(ibuffer-title-face 'modus-theme-pseudo-header)
 '(imenu-auto-rescan t)
 '(indent-tabs-mode nil)
 '(indicate-buffer-boundaries 'right)
 '(inhibit-startup-screen t)
 '(ivy-ignore-buffers
   '("\\` " "\\`\\*tramp/" "*dap-ui-*" "*iph*" "*Async-native-compile-log*" "*lsp*" "*Gidiyorum API*" "magit-process*"))
 '(ivy-use-ignore-default 'always)
 '(js-indent-level 2)
 '(kill-ring-max 100)
 '(line-spacing 0.1)
 '(lsp-file-watch-ignored-directories
   '("[/\\\\]\\.git\\'" "[/\\\\]\\.github\\'" "[/\\\\]\\.circleci\\'" "[/\\\\]\\.hg\\'" "[/\\\\]\\.bzr\\'" "[/\\\\]_darcs\\'" "[/\\\\]\\.svn\\'" "[/\\\\]_FOSSIL_\\'" "[/\\\\]\\.idea\\'" "[/\\\\]\\.ensime_cache\\'" "[/\\\\]\\.eunit\\'" "[/\\\\]node_modules" "[/\\\\]\\.yarn\\'" "[/\\\\]\\.fslckout\\'" "[/\\\\]\\.tox\\'" "[/\\\\]dist\\'" "[/\\\\]dist-newstyle\\'" "[/\\\\]\\.stack-work\\'" "[/\\\\]\\.bloop\\'" "[/\\\\]\\.metals\\'" "[/\\\\]target\\'" "[/\\\\]\\.ccls-cache\\'" "[/\\\\]\\.vscode\\'" "[/\\\\]\\.venv\\'" "[/\\\\]\\.deps\\'" "[/\\\\]build-aux\\'" "[/\\\\]autom4te.cache\\'" "[/\\\\]\\.reference\\'" "[/\\\\]\\.lsp\\'" "[/\\\\]\\.clj-kondo\\'" "[/\\\\]\\.shadow-cljs\\'" "[/\\\\]\\.babel_cache\\'" "[/\\\\]\\.cpcache\\'" "[/\\\\]\\checkouts\\'" "[/\\\\]bin/Debug\\'" "[/\\\\]obj\\'" "[/\\\\]_opam\\'" "[/\\\\]_build\\'" "[/\\\\]\\.direnv\\'" "[/\\\\]vendor" "api.gidiyorum.com/public" "api.gidiyorum.com/storage" "[/\\\\]\\.vagrant" "/home/byrmgcl/.cargo/git/checkouts"))
 '(lsp-file-watch-threshold 3000)
 '(lsp-javascript-format-enable nil)
 '(lsp-javascript-validate-enable nil)
 '(lsp-typescript-implementations-code-lens-enabled t)
 '(lsp-typescript-references-code-lens-enabled t)
 '(lsp-typescript-suggest-complete-function-calls t)
 '(lsp-ui-doc-border "#999891")
 '(lsp-ui-doc-delay 0.7)
 '(lsp-ui-doc-max-height 15)
 '(lsp-ui-flycheck-list-position 'right)
 '(lsp-ui-peek-always-show t)
 '(lsp-ui-peek-list-width 70)
 '(magit-diff-use-overlays nil)
 '(magit-todos-auto-group-items 30)
 '(magit-todos-branch-list nil)
 '(magit-todos-exclude-globs '("*.min.js"))
 '(magit-todos-keywords '("TODO" "FIXME" "TEMP" "KLUDGE" "DONT" "FAIL" "INSPECT"))
 '(magit-todos-scanner nil)
 '(magit-todos-update 350)
 '(menu-bar-mode nil)
 '(mode-icons-mode nil)
 '(mouse-buffer-menu-mode-mult 10)
 '(neo-autorefresh t)
 '(neo-create-file-auto-open t)
 '(neo-cwd-line-style 'button)
 '(neo-hide-cursor t)
 '(neo-show-slash-for-folder nil)
 '(neo-theme 'icons)
 '(neo-vc-integration '(face))
 '(neo-window-width 40)
 '(nrepl-message-colors
   '("#dc322f" "#cb4b16" "#b58900" "#5b7300" "#b3c34d" "#0061a8" "#2aa198" "#d33682" "#6c71c4"))
 '(org-agenda-files
   '("/bayram/Yandex.Disk/zetteldeft/gtd/2022-03-28-2345 | GTD - Yapılacaklar.org" "/bayram/Yandex.Disk/zetteldeft/gtd/2022-03-28-2346 | GTD - Projeler.org"))
 '(org-cliplink-max-length 75)
 '(org-edit-src-content-indentation 4)
 '(org-export-backends '(ascii html icalendar latex md odt))
 '(org-fontify-done-headline nil)
 '(org-fontify-todo-headline nil)
 '(org-footnote-section "~Dipnotlar~")
 '(org-habit-graph-column 90)
 '(org-hide-block-startup t)
 '(org-journal-carryover-delete-empty-journal 'always)
 '(org-journal-created-property-timestamp-format "%Y%m%d%V")
 '(org-journal-date-format "INC Notlar")
 '(org-journal-dir "/bayram/Yandex.Disk/zetteldeft/günlük")
 '(org-journal-file-format "%d-%m-%Y.org")
 '(org-journal-file-header
   "#+TITLE: $ %d %B %A %Y (%V. hafta)

- §2022-01-30-0138 § Ana Dizin
- §2022-03-28-2345 | GTD - Yapılacaklar

")
 '(org-journal-file-type 'daily)
 '(org-journal-time-format "%R Not: ")
 '(org-journal-time-prefix "** ")
 '(org-log-into-drawer t)
 '(org-modules '(org-bbdb org-docview org-habit org-info org-w3m))
 '(org-property-format "%-20s %s")
 '(org-random-todo-how-often 250 t)
 '(org-random-todo-mode t)
 '(org-src-lang-modes
   '(("rust" . rustic)
     ("ocaml" . tuareg)
     ("elisp" . emacs-lisp)
     ("ditaa" . artist)
     ("asymptote" . asy)
     ("dot" . fundamental)
     ("sqlite" . sql)
     ("calc" . fundamental)
     ("C" . c)
     ("cpp" . c++)
     ("C++" . c++)
     ("screen" . shell-script)
     ("shell" . sh)
     ("bash" . sh)
     ("dockerfile" . doc)))
 '(org-startup-with-inline-images t)
 '(org-structure-template-alist
   '(("s" "#+BEGIN_SRC ?

#+END_SRC")
     ("e" "#+BEGIN_EXAMPLE
?
#+END_EXAMPLE")
     ("q" "#+BEGIN_QUOTE
?
#+END_QUOTE")
     ("v" "#+BEGIN_VERSE
?
#+END_VERSE")
     ("V" "#+BEGIN_VERBATIM
?
#+END_VERBATIM")
     ("C" "#+BEGIN_CENTER
?
#+END_CENTER")
     ("c" "#+BEGIN_COMMENT
?
#+END_COMMENT")
     ("l" "#+BEGIN_EXPORT latex
?
#+END_EXPORT")
     ("L" "#+LaTeX: ")
     ("h" "#+BEGIN_EXPORT html
?
#+END_EXPORT")
     ("H" "#+HTML: ")
     ("a" "#+BEGIN_EXPORT ascii
?
#+END_EXPORT")
     ("A" "#+ASCII: ")
     ("i" "#+INDEX: ?")
     ("I" "#+INCLUDE: %file ?")
     ("b" "-----")))
 '(org-tag-faces
   '(("hata" . "red")
     ("özellik" . "green")
     ("test" . "orange")
     ("düzenleme" . "dodgerblue")
     ("planlama" . "mediumseagreen")
     ("sunucu" . "orange")
     ("diğer" . "dimgray")
     ("ev" . "dodgerblue")
     ("alışveriş" . "dimgray")
     ("yatırım" . "goldenrod")
     ("izleme" . "steelblue")
     ("araştırma" . "seagreen")
     ("okuma" . "forestgreen")
     ("fikir" . "green")
     ("gelişim" . "darkcyan")))
 '(org-tags-column 84)
 '(org-todo-keyword-faces
   '(("YAP" . "red")
     ("ATA" . "orangered")
     ("INC" . "orangered")
     ("YTA" . "orange")
     ("ITE" . "orange")
     ("YDI" . "green")
     ("ADI" . "green")
     ("IDI" . "green")
     ("IPT" . "darkslategray")
     ("PRJ" . "teal")
     ("TAM" . "green")
     ("OKU" . "red")
     ("OTA" . "orange")
     ("TKR" . "teal")
     ("ODU" . "green")))
 '(org-todo-keywords
   '((sequence "YAP(y)" "YTA(/!)" "|" "YDI(d!)")
     (sequence "INC(i)" "ITE(/!)" "|" "IDI(n@)")
     (sequence "ATA(a)" "|" "ADI(t@)")
     (sequence "OKU(o)" "OTA(!)" "|" "TKR(r!)" "ODU(u!)")
     (sequence "PRJ(j)" "|" "TAM(m@)")
     (sequence "|" "IPT(p@)")))
 '(package-archives
   '(("gnu" . "https://elpa.gnu.org/packages/")
     ("melpa" . "https://melpa.org/packages/")))
 '(package-selected-packages
   '(org-superstar org-appear centaur-tabs srcery-theme obsidian-theme nova-theme treemacs-all-the-icons treemacs-icons-dired treemacs-magit treemacs-projectile atom-one-dark-theme material-theme humanoid-themes monokai-theme nimbus-theme good-scroll better-scroll ace-window alert async autothemer biblio biblio-core bind-key bui cfrs concurrent ctable dash dash-functional deferred dired-hacks-utils direx docker-tramp edbi elpy emacsql emacsql-sqlite emmet-mode epc epl f flx flycheck font-utils frame-local git-commit gntp grizzl header2 highlight-indentation ht htmlize http-post-simple hydra ivy js2-mode json-reformat json-snatcher key-chord let-alist lib-requires list-utils log4e lsp-treemacs lv magit magit-popup magit-section map markdown-mode memoize mode-icons org org-agenda-property org-ql org-sticky-header org-super-agenda ov parsebib pcache pcre2el pdf-tools peg persistent-soft pfuture pkg-info popup pos-tip posframe powerline project projectile pythonic pyvenv racer request request-deferred rust-mode s seq spinner tablist tern transient treemacs ts typescript-mode ucs-utils web-server websocket wgrep with-editor xcscope xref xterm-color yaoddmuse yasnippet solarized-theme lsp-rust rustic org-journal org-cliplink org-randomnote org-random-todo zetteldeft org-bullets dream-theme dap-mode py-autopep8 python-mode gitignore-mode git-gutter flx-ido rich-minority js-import lsp-ivy lsp-ui which-key lsp-jedi use-package lsp-mode ron-mode pyenv-mode company-anaconda anaconda-mode deft css-eldoc org-sidebar magit-todos php-mode centered-cursor-mode diminish-buffer ivy-xref moe-theme company-racer spacemacs-theme dockerfile-mode all-the-icons rg rjsx-mode beacon smex prettier-js add-node-modules-path yasnippet-snippets php-cs-fixer paganini-theme counsel swiper circadian yaml-mode py-isort multiple-cursors ede-php-autoload company dumb-jump focus anzu avy tangotango-theme tommyh-theme zonokai-theme imenu+ web-mode web-beautify unicode-fonts toc-org tj-mode therapy sudo-edit sphinx-doc restclient redis rainbow-mode rainbow-delimiters python-info pylint pydoc popup-complete poporg phpcbf pcmpl-pip pcmpl-git ox-twbs ox-rst orgit org-redmine org-readme org-password-manager org-magit org-download org-doing org-dashboard org-autolist nodejs-repl nav-flash move-text menu-bar+ markdown-preview-mode magit-tramp magit-gitflow kivy-mode json-mode js2-highlight-vars idomenu hl-todo historyf highlight-blocks highlight goose-theme git-timemachine fontawesome flycheck-tip flycheck-rust flycheck-pos-tip fill-column-indicator fic-mode eredis duplicate-thing docker dired-rainbow dired-k diff-hl cargo c-eldoc))
 '(pdf-view-midnight-colors '("#b2b2b2" . "#292b2e"))
 '(php-cs-fixer-rules-fixer-part-options
   '("no_multiline_whitespace_before_semicolons" "-concat_space" "-blank_line_before_statement" "-doctrine_annotation_indentation"))
 '(php-cs-fixer-rules-level-part-options '("@PSR1" "@PSR2"))
 '(php-extras-auto-complete-insert-parenthesis nil)
 '(php-mode-coding-style 'psr2)
 '(php-project-use-projectile-to-detect-root t)
 '(pos-tip-background-color "#073642")
 '(pos-tip-foreground-color "#272822")
 '(prettier-js-command "prettier-eslint")
 '(projectile-completion-system 'ivy)
 '(projectile-tags-backend 'xref)
 '(py-imenu-create-index-function 'py-imenu-create-index-function)
 '(py-imenu-create-index-p t)
 '(py-imenu-show-method-args-p t)
 '(py-install-directory "/home/byrmgcl/.pyenv/versions")
 '(py-pylint-command-args '(""))
 '(pylint-options '(""))
 '(python-environment-directory "~/.pyenv/versions")
 '(python-environment-virtualenv
   '("virtualenv" "--system-site-packages" "--quiet" "--python" "/usr/bin/python3"))
 '(python-indent-guess-indent-offset nil)
 '(python-indent-offset 4)
 '(python-shell-interpreter "python3")
 '(racer-cmd "~/.cargo/bin/racer")
 '(racer-rust-src-path
   "/home/byrmgcl/.rustup/toolchains/stable-x86_64-unknown-linux-gnu")
 '(scroll-bar-mode nil)
 '(sentence-end-double-space nil)
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#073642" 0.2))
 '(smooth-scrolling-mode t)
 '(tab-width 4)
 '(term-default-bg-color "#002b36")
 '(term-default-fg-color "#839496")
 '(tern-ac-on-dot nil)
 '(tool-bar-mode nil)
 '(tool-bar-position 'left)
 '(tooltip-mode nil)
 '(treemacs-file-follow-delay 0.7)
 '(treemacs-width 44)
 '(undo-limit 200000)
 '(undo-outer-limit 30000000)
 '(undo-strong-limit 300000)
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   '((20 . "#dc322f")
     (40 . "#ffffa21b0000")
     (60 . "#ffffd2170000")
     (80 . "#b58900")
     (100 . "#fffffffe0000")
     (120 . "#fffffffe0000")
     (140 . "#fffffffe0000")
     (160 . "#fffffffe0000")
     (180 . "#859900")
     (200 . "#dc61ffb77bfe")
     (220 . "#c516ffa79f16")
     (240 . "#a726ffaac017")
     (260 . "#7bfcffc6e035")
     (280 . "#2aa198")
     (300 . "#0000fffffffe")
     (320 . "#0000fffffffe")
     (340 . "#0000fffffffe")
     (360 . "#268bd2")))
 '(vc-annotate-very-old-color nil)
 '(warning-suppress-log-types '((comp) (comp) (comp) (comp) (comp)))
 '(warning-suppress-types '((comp) (comp) (comp) (comp)))
 '(web-mode-code-indent-offset 2)
 '(web-mode-markup-indent-offset 2)
 '(weechat-color-list
   '(unspecified "#002b36" "#073642" "#a7020a" "#dc322f" "#5b7300" "#859900" "#866300" "#b58900" "#0061a8" "#268bd2" "#a00559" "#d33682" "#007d76" "#2aa198" "#839496" "#657b83"))
 '(which-key-idle-delay 0.7)
 '(which-key-mode t)
 '(whitespace-style
   '(face trailing tabs newline empty indentation::tab indentation::space indentation space-after-tab::tab space-after-tab::space space-after-tab space-before-tab::tab space-before-tab::space space-before-tab))
 '(window-divider-default-places 'bottom-only)
 '(xterm-color-names
   ["#2f2f2e" "#ffb4ac" "#8ac6f2" "#e5c06d" "#a4b5e6" "#e5786d" "#7ec98f" "#e8e5db"])
 '(xterm-color-names-bright
   ["#2a2a29" "#ddaa6f" "#6a6a65" "#74736f" "#8d8b86" "#834c98" "#999891" "#f6f3e8"])
 '(zetteldeft-backlink-prefix "# ⏎: ")
 '(zetteldeft-tag-line-prefix "# Etiketler:")
 '(zetteldeft-tag-prefix "#")
 '(zetteldeft-tag-regex "[#@][[:alpha:]_-]+")
 '(zetteldeft-title-prefix "#+TITLE: ")
 '(zetteldeft-title-suffix
   "

# Etiketler: #_gelen #__düzenlenmekte



* COMMENT ~Yorumlar~
* ~Kaynaklar~
"))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;; Global
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-to-list 'load-path "/home/byrmgcl/.emacs.d/vendor")

;; (setq sml/theme 'dark)
;; (setq sml/theme 'respectful)
;; (sml/setup)

(good-scroll-mode)

;; (defun set-exec-path-from-shell-PATH ()
;;   "Set up Emacs' `exec-path' and PATH environment variable to match
;; that used by the user's shell.

;; This is particularly useful under Mac OS X and macOS, where GUI
;; apps are not started from a shell."
;;   (interactive)
;;   (let ((path-from-shell (replace-regexp-in-string
;;               "[ \t\n]*$" "" (shell-command-to-string
;;                       "$SHELL --login -c 'echo $PATH'"
;;                             ))))
;;     (setenv "PATH" path-from-shell)
;;     (setq exec-path (split-string path-from-shell path-separator))))

;; (set-exec-path-from-shell-PATH)


(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5    ; and how many of the old
  )

;; (global-centered-cursor-mode)

;; Ben yazdımya la bunu :)
(defun video-time-to-second (start end)
  (interactive "r")
    (if (use-region-p)
        (let ((regionp (buffer-substring start end)))
          (setq time (split-string regionp ":"))
          (setq n0 (string-to-number (nth 0 time)))
          (setq n1 (string-to-number (nth 1 time)))
          (setq time-len (length time))
          (cond ((eq time-len 2)
                 (setq result-m (* n0 60))
                 (setq result (+  result-m n1))
                 (message "%s => %s saniye" regionp result)))
          (cond ((eq time-len 3)
                 (setq n2 (string-to-number (nth 2 time)))
                 (setq result-m (* n1 60))
                 (setq result-h (* n0 3600))
                 (setq result-t (+ result-h result-m))
                 (setq result (+  result-t n2))
                 (message "%s g=> %s saniye" regionp result)))
          )))
(global-set-key (kbd "M-ş m") 'video-time-to-second)
(projectile-mode)
(setq rm-blacklist '(" ws" " ARev" " Rbow" " WK" " pair" " company" " Anzu"
                     " Prettier" " $" " Emmet" " Abbrev" " ElDoc" " hs"
                     " GitGutter" " Autolist"))
(rich-minority-mode)

(global-set-key (kbd "M-ş d") 'deft)
(global-set-key (kbd "C-z") 'deft)

(defhydra projectile-hydra (:color blue :columns 3)
  "M-ş p"
  ("p" treemacs-switch-workspace "Treemacs switch workspace")
  ("e" treemacs-edit-workspaces "Treemacs edit workspaces")
  ("P" projectile-switch-project "Switch project")
  ("<left>" projectile-previous-project-buffer "Previous project buffer")
  ("<right>" projectile-next-project-buffer "Next project buffer")
  ("r" projectile-replace "Replace")
  ("k" projectile-kill-buffers "Kill buffers")
  ("f" projectile-find-file "Find file")
  ("b" projectile-switch-to-buffer "Switch to buffer")
  ("s" projectile-save-project-buffers "Save project buffers")
  ("i" projectile-ibuffer "Ibuffer")
  ("o" projectile-project-buffers-other-buffer "Project buffers other buffer")
  ("d" projectile-display-buffer "Display buffer"))
(global-set-key (kbd "M-ş p") 'projectile-hydra/body)

(require 'centaur-tabs)
(centaur-tabs-mode t)
(centaur-tabs-headline-match)
(centaur-tabs-group-by-projectile-project)
;; (centaur-tabs-toggle-groups)

(setq centaur-tabs-height 24)
(setq centaur-tabs-style "bar")
(setq centaur-tabs-set-icons t)
(setq centaur-tabs-set-bar 'over)
(setq centaur-tabs-set-modified-marker t)
(setq centaur-tabs-label-fixed-length 24)

(global-set-key (kbd "M-ö")  'previous-buffer)
(global-set-key (kbd "M-ç") 'next-buffer)
(global-set-key (kbd "M-C-ö") 'centaur-tabs-backward)
(global-set-key (kbd "M-C-ç") 'centaur-tabs-forward)

(defun show-file-name ()
  "Show the full path file name in the minibuffer."
  (interactive)
  (message (buffer-file-name)))
(global-set-key (kbd "M-ş M-ş") 'show-file-name)
(global-set-key (kbd "M-ş ş") 'what-cursor-position)


(add-hook 'prog-mode-hook 'display-fill-column-indicator-mode)

(require 'all-the-icons)
;; (autoload 'all-the-icons "all-the-icons")

;; (global-diff-hl-mode)
(global-anzu-mode)

(add-hook 'prog-mode-hook 'rainbow-mode)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(add-hook 'prog-mode-hook 'hs-minor-mode)
;; (add-hook 'prog-mode-hook 'autopair-mode)
;; (add-hook 'prog-mode-hook 'flycheck-mode)
(add-hook 'prog-mode-hook 'fic-mode)
(add-hook 'prog-mode-hook 'yas-minor-mode)

(defhydra magit-hydra (:color blue :columns 3)
  "M-ğ"
  ("d" duplicate-thing "Duplicate thing")
  ("r" anzu-query-replace "Anzu query replace")
  ("F" focus-mode "Focus mode")
  ("f" magit-gitflow-popup "Magit git flow popup")
  ("s" magit-status "Magit status")
  ("t" git-timemachine-toggle "Git timemachine toggle")
  ("b" magit-blame-addition "Magit blame addition")
  ("B" magit-blame-mode "Magit blame mode")
  ("c" magit-cherry "Magit cherry")
  ("l" counsel-git-log "Counsel git log")
  ("v" toggle-truncate-lines "Toggle truncate lines"))
(global-set-key (kbd "M-ğ") 'magit-hydra/body)

(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
                    ((numberp (cdr alpha)) (cdr alpha))
                    ;; Also handle undocumented (<active> <inactive>) form.
                    ((numberp (cadr alpha)) (cadr alpha)))
              100)
         '(95 . 95) '(100 . 100)))))
(defun transparency (value)
  "Sets the transparency of the frame window. 0=transparent/100=opaque"
  (interactive "nTransparency Value 0 - 100 opaque:")
  (set-frame-parameter (selected-frame) 'alpha value))
(global-set-key (kbd "M-ş M-t") 'toggle-transparency)

(global-set-key (kbd "M-ş r") 'replace-string)
(global-set-key (kbd "M-ş M-w") 'whitespace-cleanup)
(global-set-key (kbd "M-ş w") 'woman)

;; (global-set-key (kbd "M-ş M-o") 'other-frame)
(global-set-key (kbd "M-ş t") 'shell)
;; (global-set-key (kbd "ш") 'delete-backward-char)
(global-set-key (kbd "C-ı") 'delete-backward-char)
(global-set-key (kbd "M-ı") 'backward-kill-word)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key (kbd "C-<mouse-8>") 'previous-buffer)
(global-set-key (kbd "C-<mouse-9>") 'next-buffer)
(global-set-key (kbd "C-S-<mouse-8>") 'centaur-tabs-backward)
(global-set-key (kbd "C-S-<mouse-9>") 'centaur-tabs-forward)
(global-set-key (kbd "<S-mouse-8>") 'centaur-tabs-backward-group)
(global-set-key (kbd "<S-mouse-9>") 'centaur-tabs-forward-group)

;; (global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)

(global-set-key (kbd "C-M-<mouse-9>") 'hs-hide-block)
(global-set-key (kbd "C-M-<mouse-8>") 'hs-show-block)
;; (global-set-key (kbd "C-M-S-<mouse-1>") 'mc/add-cursor-on-click)

(global-set-key (kbd "<C-M-mouse-1>") 'mouse-buffer-menu)
(global-set-key (kbd "<C-M-mouse-2>") 'menu-bar-open)
(global-set-key (kbd "<C-M-mouse-3>") 'centaur-tabs--groups-menu)

(global-set-key (kbd "<C-S-mouse-1>") 'zetteldeft-follow-link)
;; (global-set-key (kbd "<C-S-mouse-2>") 'lsp-treemacs-symbols)
(global-set-key (kbd "<C-S-mouse-3>") 'mc/add-cursor-on-click)

(global-set-key (kbd "<M-S-mouse-1>") 'delete-other-windows)
(global-set-key (kbd "<M-S-mouse-2>") 'kill-this-buffer)
(global-set-key (kbd "<M-S-mouse-3>") 'delete-window)

(global-set-key (kbd "<M-S-mouse-8>") 'mouse-split-window-vertically)
(global-set-key (kbd "<M-S-mouse-9>") 'mouse-split-window-horizontally)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)

(setenv "PATH" (concat "/usr/local/bin:" (getenv "PATH")))

;; pencereler arasında gezin
(global-set-key (kbd "M-ş M-l") 'windmove-left)          ; move to left window
(global-set-key (kbd "M-ş M-r") 'windmove-right)        ; move to right window
(global-set-key (kbd "M-ş M-u") 'windmove-up)              ; move to upper window
(global-set-key (kbd "M-ş M-d") 'windmove-down)          ; move to downer window

(global-hl-todo-mode)

(global-set-key (kbd "M-ş s") 'treemacs)
(with-eval-after-load 'treemacs
  (treemacs-toggle-show-dotfiles)
  (treemacs-follow-mode)
  (treemacs-icons-dired-mode)
  ;; (treemacs-tag-follow-mode)
  )
;; (global-set-key (kbd "M-ş M-s") 'treemacs)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Multiple Cursors ;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key (kbd "M-ü n l") 'mc/mark-next-like-this)
(global-set-key (kbd "M-ü n w") 'mc/mark-next-like-this-word)

(global-set-key (kbd "M-ü p l") 'mc/mark-previous-like-this)
(global-set-key (kbd "M-ü p w") 'mc/mark-previous-like-this-word)

(global-set-key (kbd "M-ü a d") 'mc/mark-all-dwim)
(global-set-key (kbd "M-ü a l") 'mc/mark-all-like-this-dwim)
(global-set-key (kbd "M-ü a t") 'mc/mark-all-like-this)
(global-set-key (kbd "M-ü a w") 'mc/mark-all-words-like-this)
(global-set-key (kbd "M-ü d l") 'mc/mark-all-like-this-in-defun)
(global-set-key (kbd "M-ü d w") 'mc/mark-all-words-like-this-in-defun)

(global-set-key (kbd "M-ü l l") 'mc/edit-lines)
(global-set-key (kbd "M-ü l b") 'mc/edit-beginnings-of-lines)
(global-set-key (kbd "M-ü l e") 'mc/edit-ends-of-lines)

(global-set-key (kbd "M-ü n u") 'mc/unmark-next-like-this)
(global-set-key (kbd "M-ü p u") 'mc/unmark-previous-like-this)

;; (global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;ELISP
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-hook 'emacs-lisp-mode-hook 'eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'eldoc-mode)
(add-hook 'ielm-mode-hook 'eldoc-mode)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;hide show
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defhydra hideshow-hydra (:color blue :columns 3)
  "C-ş"
  ("e" enlarge-window-horizontally  "Enlarge window horizontally")
  ("v" enlarge-window  "Enlarge window vertically")
  ("C-a" hs-hide-all "Hide all")
  ("a" hs-show-all "Show all")
  ("C-b" hs-hide-block "Hide block")
  ("b" hs-show-block "Show block")
  ("C-t" hs-toggle-hiding "Toggle hiding")
  ("C-l" hs-hide-level "Hide level")
  )
(global-set-key (kbd "C-ş") 'hideshow-hydra/body)


(defun ido-kill-emacs-hook ()
  (ignore-errors (ido-save-history)))
(ido-kill-emacs-hook)
(ido-mode)
(ido-everywhere)
(flx-ido-mode)
;; (setq ido-enable-flex-matching t
;;       ido-use-filename-at-point nil
;;       ido-auto-merge-work-directories-length 0
;; ;; Allow the same buffer to be open in different frames
;;       ido-default-buffer-method 'selected-window)
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; ;;;;;;;;;;;;;;;;;;Python
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'python-mode)
;; (add-hook 'python-mode-hook 'py-autopep8-enable-on-save)
(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
(add-to-list 'auto-mode-alist '("\\.pyw\\'" . python-mode))

(autoload 'pylint "pylint")
(add-hook 'python-mode-hook 'pylint-add-menu-items)
(add-hook 'python-mode-hook 'pylint-add-key-bindings)
;; (add-hook 'python-mode-hook
;;           #'(lambda ()
;;               (setq autopair-handle-action-fns
;;                     (list #'autopair-default-handle-action
;;                           #'autopair-python-triple-quote-action))))

(setq exec-path (append exec-path '("~/.pyenv/bin")))
(pyenv-mode)
(global-set-key (kbd "C-c ö") 'pyenv-mode-set)
(global-set-key (kbd "C-c C-ö") 'pyenv-mode-unset)

(require 'pyenv-mode)
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;; LSP Mode ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq lsp-keymap-prefix "M-ş M-ş") ;; order important

(setq read-process-output-max (* 1024 1024))
(setq gc-cons-threshold 100000000)


(add-hook 'prog-mode-hook 'whitespace-mode)
(require 'dap-php)
(require 'dap-node)
(require 'dap-firefox)

;; (setq dap-auto-configure-features '(sessions locals controls tooltip))
(add-hook 'dap-stopped-hook
          #'(lambda (arg) (call-interactively #'dap-hydra)))
(dap-mode)
(dap-php-setup)
(global-set-key (kbd "M-g d") 'dap-hydra)
(global-set-key (kbd "M-g t") 'dap-tooltip-at-point)

;; ;; The modes below are optional

;; (dap-ui-mode 1)
;; ;; enables mouse hover support
;; (dap-tooltip-mode 1)
;; ;; use tooltips for mouse hover
;; ;; if it is not enabled `dap-mode' will use the minibuffer.
;; (tooltip-mode 1)
;; ;; displays floating panel with debug buttons
;; ;; requies emacs 26+
;; (dap-ui-controls-mode 1)
(with-eval-after-load 'dap-php
  (dap-register-debug-template "PHP debug with custom path"
                               (list :type "php"
                                     :cwd nil
                                     :request "launch"
                                     :name "Php Debug with path"
                                     :args '("")
                                     :pathMappings (ht ("/vagrant/" "/home/byrmgcl/dev/gidiyorum/api.gidiyorum.com/"))
                                     :stopOnEntry t
                                     :sourceMaps t)))



(add-hook 'python-mode-hook 'lsp-mode)
(add-hook 'rustic-mode-hook 'lsp-mode)
(add-hook 'php-mode-hook 'lsp-mode)
(add-hook 'web-mode-hook 'lsp-mode)
(add-hook 'rjsx-mode-hook 'lsp-mode)


(add-hook 'python-mode-hook 'lsp-deferred)
(add-hook 'php-mode-hook 'lsp-deferred)
(add-hook 'web-mode-hook 'lsp-deferred)
(add-hook 'rjsx-mode-hook 'lsp-deferred)
(add-hook 'rustic-mode-hook 'lsp-deferred)

(use-package lsp-mode
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration))
  :custom
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-rust-analyzer-server-display-inlay-hints t)
  :commands (lsp))

;; ;; optional if you want which-key integration
(use-package which-key
  :config
  (which-key-mode))

(use-package lsp-ui :commands lsp-ui-mode)

(defhydra lsp-hydra (:color blue :columns 3)
    "C-ğ"
    ("i" lsp-ui-imenu "Lsp ui imenu")
    ("d" lsp-describe-thing-at-point "Lsp describe thing at point")
    ("f" lsp-format-buffer "Lsp format buffer")
    ("t" lsp-treemacs-symbols "Lsp treemacs symbols")
    ("u" lsp-ui-sideline-toggle-symbols-info "Lsp ui sideline toggle symbols info")
    ("s" lsp-ivy-workspace-symbol "Lsp ivy workspace symbol"))
(global-set-key (kbd "C-ğ") 'lsp-hydra/body)


(add-hook 'lsp-ui-mode-hook
          #'(lambda ()
              (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
              (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
              ))
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Rust
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'rustic)
(define-key rustic-mode-map (kbd "C-M-ş") #'company-indent-or-complete-common)
(define-key rustic-mode-map (kbd "C-c C-c C-r") 'lsp-rust-analyzer-run)
(define-key rustic-mode-map (kbd "C-c C-c <tab>") 'lsp-rust-analyzer-rerun)
(define-key rustic-mode-map (kbd "C-c C-c C-s") 'lsp-rust-analyzer-status)
(setq company-tooltip-align-annotations t)
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; ;;;;;;;;;;;SGML, HTML and CSS, XML, JavaScript
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.htm\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.twig\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.dtl\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tmpl\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.test.js\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js[x]?\\'" . rjsx-mode))

(setq web-mode-engines-alist
      '(("jinja2"    . "\\.jinja2\\'")
        ("django"    . "\\.html\\'")))

(add-hook 'web-mode-hook 'my-web-mode-hook)
(defun my-web-mode-hook ()
  "Hooks for Web mode."

  (setq ac-auto-start nil)
  (define-key web-mode-map (kbd "C-c s") 'web-mode-snippet-insert)
  (define-key web-mode-map (kbd "C-c M-d n") 'web-mode-dom-normalize)
  (define-key web-mode-map (kbd "C-c M-d t") 'web-mode-dom-traverse)
  (define-key web-mode-map (kbd "C-c M-d x") 'web-mode-dom-xpath)
  (define-key web-mode-map (kbd "C-c M-d q") 'web-mode-dom-quotes-replace)
  (define-key web-mode-map (kbd "C-c M-d e") 'web-mode-dom-entities-replace)
  (define-key web-mode-map (kbd "C-c M-d d") 'web-mode-dom-errors-show)
  (define-key web-mode-map (kbd "C-c /") 'web-mode-element-close)
  )

;; CSS
(add-hook 'css-mode-hook 'css-eldoc-enable)
(add-hook 'css-mode-hook 'emmet-mode)

(add-hook 'nxml-mode-hook 'emmet-mode)


;; JavaScript
(add-hook 'web-mode-hook 'emmet-mode)
(add-hook 'rjsx-mode-hook 'emmet-mode)

(require 'flycheck)
;; disable jshint since we prefer eslint checking
(setq-default flycheck-disabled-checkers
  (append flycheck-disabled-checkers
    '(javascript-jshint)))

;; use eslint with web-mode for jsx files
(flycheck-add-mode 'javascript-eslint 'web-mode)

;; customize flycheck temp file prefix
(setq-default flycheck-temp-prefix ".flycheck")

;; disable json-jsonlist checking for json files
(setq-default flycheck-disabled-checkers
  (append flycheck-disabled-checkers
    '(json-jsonlist)))

;; JavaScript
(add-hook 'js2-mode-hook 'prettier-js-mode)
(eval-after-load 'js2-mode
  '(progn
     (setq ac-auto-start nil)
     (add-hook 'js2-mode-hook #'add-node-modules-path)
     )
  )
(eval-after-load 'web-mode
  '(add-hook 'web-mode-hook #'add-node-modules-path))
(add-hook 'js2-mode-hook
          #'(lambda ()
              (define-key js2-mode-map "\C-cr" 'rjsx-rename-tag-at-point)))
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;





;;;;;;;;;;;;;;;Magit;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-git-gutter-mode)
(global-set-key (kbd "M-g M-p") 'git-gutter:previous-hunk)
(global-set-key (kbd "M-g M-n") 'git-gutter:next-hunk)
(add-hook 'magit-mode-hook 'turn-on-magit-gitflow)
(magit-todos-mode)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; ;;;;;;Semantic and Ivy and Smex and dumb-jump
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key (kbd "M-g j") 'lsp-ui-peek-find-definitions)
(global-set-key (kbd "M-g r") 'lsp-ui-peek-find-references)
(global-set-key (kbd "M-g b") 'xref-pop-marker-stack)


(ivy-mode)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; This is your old M-x.
(global-set-key (kbd "M-ş C-c M-x") 'execute-extended-command)
(global-set-key (kbd "C-s") 'swiper)
(global-set-key (kbd "C-r") 'ivy-resume)
(global-set-key (kbd "M-ş C-s") 'isearch-forward)
(global-set-key (kbd "M-ş C-r") 'isearch-backward)
(global-set-key (kbd "C-x b") 'ivy-switch-buffer)

(global-set-key (kbd "M-y") 'counsel-yank-pop)
(global-set-key (kbd "M-ş M-f") 'counsel-find-file)
(global-set-key (kbd "<f6>") 'ivy-resume)
(global-set-key (kbd "M-ş f") 'counsel-git)

(defhydra counsel-hydra (:color blue :columns 3)
    "M-ş c"
    ("b" counsel-bookmark "Counsel bookmark")
    ("s" counsel-switch-buffer "Counsel switch buffer")
    ("j" counsel-file-jump "Counsel file jump")
    ("i" counsel-imenu "Counsel imenu")
    ("r" counsel-recentf "Counsel recentf")
    ("A" counsel-linux-app "Counsel linux app")
    ("f" counsel-flycheck "Counsel flycheck")
    ("c" counsel-colors-web "Counsel colors web")
    ("d" counsel-descbinds "Counsel descbinds")
    ("F" counsel-fontawesome "Counsel fontawesome")
    ("t" counsel-tracker "Counsel tracker")
    ("u" counsel-unicode-char "Counsel unicode char")
    ("a" all-the-icons-insert "All the icons insert")
    ("m" counsel-tmm "Counsel tmm"))
(global-set-key (kbd "M-ş c") 'counsel-hydra/body)

(global-set-key (kbd "M-ş g") 'counsel-git-grep)
(global-set-key (kbd "M-ş M-g") 'counsel-rg)

(defhydra highlight-hydra (:color blue :columns 3)
    "M-ş h"
    ("i" highlight-indentation-mode "Highlight indentation mode")
    ("b" highlight-blocks-now "Highlight blocks now"))
(global-set-key (kbd "M-ş h") 'highlight-hydra/body)
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; ;;;;;;;;;;;Company and Avy
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-company-mode)
(eval-after-load 'company
  '(progn
     (define-key company-mode-map (kbd "C-M-ş") 'company-complete-common-or-cycle)))
(defun my-company-visible-and-explicit-action-p ()
  (and (company-tooltip-visible-p)
       (company-explicit-action-p)))

(defun company-ac-setup ()
  "Sets up `company-mode' to behave similarly to `auto-complete-mode'."
  (setq company-require-match nil)
  (setq company-auto-complete #'my-company-visible-and-explicit-action-p)
  (setq company-frontends '(company-echo-metadata-frontend
                            company-pseudo-tooltip-unless-just-one-frontend-with-delay
                            company-preview-frontend))
  (define-key company-active-map [tab]
    'company-select-next-if-tooltip-visible-or-complete-selection)
  (define-key company-active-map (kbd "C-M-ş")
    'company-select-next-if-tooltip-visible-or-complete-selection))

(with-eval-after-load 'company
  (define-key company-active-map (kbd "M-n") #'company-select-next)
  (define-key company-active-map (kbd "M-p") #'company-select-previous)
  (define-key company-active-map (kbd "C-n") #'company-select-next)
  (define-key company-active-map (kbd "C-p") #'company-select-previous))

(company-ac-setup)

(beacon-mode) ;; higlight cursor

(global-set-key (kbd "C-:") 'avy-goto-char)
(global-set-key (kbd "C-'") 'avy-goto-char-2)
(global-set-key (kbd "M-g f") 'avy-goto-line)
(global-set-key (kbd "M-g w") 'avy-goto-word-1)
(global-set-key (kbd "M-g e") 'avy-goto-word-0)
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; ;;;;;;;;;;;Others
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;PHP
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'auto-mode-alist '("\\.php$'" . php-mode))
(add-to-list 'auto-mode-alist '("\\.inc$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.psp$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.ctp$" . php-mode))

(add-hook 'before-save-hook 'php-cs-fixer-before-save)
(add-hook 'php-mode-hook
          #'(lambda ()
             ;; Enable company-mode
             (company-mode t)
             ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu/mu4e")



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;ORG-MODE;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'org-journal)

(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))

(setq inhibit-compacting-font-caches t)

(load-library "find-lisp")
(setq org-randomnote-candidates
      ;; (find-lisp-find-files "/bayram/Yandex.Disk/zetteldeft/orman" "\.org$")
      (find-lisp-find-files "/bayram/Yandex.Disk/zetteldeft" "\.org$"))
(eval-after-load 'org
  (lambda ()
    (require 'zetteldeft)
    (org-link-set-parameters
     "zdlink"
     :follow
     (lambda (str) (zetteldeft--search-filename
            (zetteldeft--lift-id str)))
     :complete  #'sodaware/zd-complete-link
     :help-echo "Searches provided ID in Zetteldeft")))

(defun sodaware/zd-complete-link ()
  "Link completion for `zdlink' type links."
  (let* ((file (completing-read "File to link to: "
                (deft-find-all-files-no-prefix)))
     (link (zetteldeft--lift-id file)))
    (unless link (user-error "No file selected"))
    (concat "zdlink:" link)))

(setq zetteldeft-home-id "2022-01-30-0138")

;; (defun zetteldeft-insert-link-org-style-byrmgcl (id &optional title)
;;   "Insert a Zetteldeft link in Org-mode format as zdlink: type."
;;   (if title
;;       (insert "[[zdlink:" id "][" title "]]")
;;     (insert "[[zdlink:" id "][§]]")))

(zetteldeft-set-classic-keybindings)
(defun efls/deft-open-other ()
 (interactive)
 (deft-open-file-other-window t))
(defun efls/deft-open-preview ()
 (interactive)
 (deft-open-file-other-window))

(with-eval-after-load 'deft
  (define-key deft-mode-map
    (kbd "<tab>") 'efls/deft-open-preview)
  (define-key deft-mode-map
    (kbd "<s-return>") 'efls/deft-open-other)
  (define-key deft-mode-map
    (kbd "s-j") 'evil-next-line)
  (define-key deft-mode-map (kbd "s-k") 'evil-previous-line))

(defhydra org-hydra (:color blue :columns 3)
    "M-ş o"
    ("s" org-sidebar-toggle "Org sidebar toggle")
    ("S" my/org-sort-entries "Org sort entries")
    ("d" org-journal-new-date-entry "Org journal new date entry")
    ("t" counsel-org-tag "Counsel org tag")
    ("T" org-sidebar-tree-toggle "Org sidebar tree toggle")
    ("a" org-agenda "Org agenda")
    ("r" org-randomnote "Org random note")
    ("g" org-random-todo "Org random GTD")
    ("h" counsel-org-agenda-headlines "Counsel org agenda headlines")
    ("c" org-columns "Org columns")
    ("m" counsel-org-capture "Counsel org capture")
    ("j" org-journal-new-entry "Org journal new entry")
    ("o" org-journal-open-current-journal-file "Org journal open current file")
    ("l" org-cliplink "Org clip link")
    ("L" org-journal-list--start "Org journal list")
    ("e" counsel-org-entity "Counsel org entity")
    ("M" org-capture-goto-target "Org capture go to target"))
(global-set-key (kbd "M-ş o") 'org-hydra/body)

;; (defun zd-zetteldeft ()
;;   (interactive)
;; (deft)
;;   (setq deft-directory "/bayram/Yandex.Disk/zetteldeft")
;;   (deft-refresh))

;; (defun zd-notlar ()
;;   (interactive)
;; (deft)
;;   (setq deft-directory "/bayram/Yandex.Disk/zetteldeft/notlar")
;;   (deft-refresh))

;; (defun zd-telefon ()
;; (deft)
;;   (interactive)
;;   (setq deft-directory "/bayram/Yandex.Disk/zetteldeft/telefon")
;;   (deft-refresh))

;; (defun zd-journal ()
;; (deft)
;;   (interactive)
;;   (setq deft-directory "/bayram/Yandex.Disk/zetteldeft/günlük")
;;   (deft-refresh))

;; (defun zd-archive ()
;; (deft)
;;   (interactive)
;;   (setq deft-directory "/bayram/Yandex.Disk/zetteldeft/arşiv")
;;   (deft-refresh))

;; (defun zd-orman ()
;; (deft)
;;   (interactive)
;;   (setq deft-directory "/bayram/Yandex.Disk/zetteldeft/orman")
;;   (deft-refresh))

;; (defun zd-it ()
;; (deft)
;;   (interactive)
;;   (setq deft-directory "/bayram/Yandex.Disk/zetteldeft/bilişim")
;;   (deft-refresh))

;; (defun zd-todo ()
;; (deft)
;;   (interactive)
;;   (setq deft-directory "/bayram/Yandex.Disk/zetteldeft/yapılacak")
;;   (deft-refresh))

;; ;(require 'hydra)
;; (defhydra zd-switch (:color blue :columns 4)
;;   "C-c d d"
;;   ("z" zd-zetteldeft "Zetteldeft")
;;   ("n" zd-notlar "Notlar")
;;   ("g" zd-journal "Günlük")
;;   ("o" zd-orman "Orman")
;;   ("b" zd-it "Bilişim")
;;   ("y" zd-todo "Yapılacak")
;;   ("t" zd-telefon "Telefon")
;;   ("a" zd-archive "Arşiv"))
;; (global-set-key (kbd "C-c d d") 'zd-switch/body)


(add-hook 'org-mode-hook 'auto-fill-mode)
(add-hook 'org-mode-hook (lambda () (org-autolist-mode)))
(add-hook 'org-mode-hook #'org-sticky-header-mode)
;; (add-hook 'org-mode-hook 'display-fill-column-indicator-mode)
(add-hook 'org-mode-hook (lambda () (set-fill-column 93)))

(add-hook 'org-mode-hook 'my-org-org-mode)
(defun my-org-org-mode ()
  (define-key org-mode-map (kbd "C-c s") 'org-schedule)
  )

(defun zetteldeft-org-journal-setup ()
  "Add font-lock highlighting for zetteldeft links.
Called when `zetteldeft-link-indicator' or
`zetteldeft-id-regex' are customized."
  (when (and (boundp 'zetteldeft-link-indicator)
             (boundp 'zetteldeft-id-regex)
             (boundp 'zetteldeft-link-suffix))
     (font-lock-remove-keywords 'org-journal-mode
        `((,(concat zetteldeft-link-indicator
                    zetteldeft-id-regex
                    zetteldeft-link-suffix)
           . font-lock-warning-face))))
  (when (and (boundp 'zetteldeft-id-regex)
             (boundp 'zetteldeft-link-indicator)
             (boundp 'zetteldeft-link-suffix))
     (font-lock-add-keywords 'org-journal-mode
        `((,(concat zetteldeft-link-indicator
                    zetteldeft-id-regex
                    zetteldeft-link-suffix)
           . font-lock-warning-face)))))
(zetteldeft-org-journal-setup)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;FlyCheck
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-hook 'flycheck-mode-hook #'(lambda ()(flycheck-define-checker python-pylint
                                           "A Python syntax and style checker using Pylint.
This syntax checker requires Pylint 1.0 or newer.
See URL `http://www.pylint.org/'."
                                           ;; -r n disables the scoring report
                                           :command ("pylint"
                                                     "--msg-template" "{path}:{line}:{column}:{C}:{msg_id}:{msg}"
                                                     (config-file "--rcfile" flycheck-pylintrc)
                                                     ;; Need `source-inplace' for relative imports (e.g. `from .foo
                                                     ;; import bar'), see https://github.com/flycheck/flycheck/issues/280
                                                     source-inplace)
                                           :error-filter
                                           (lambda (errors)
                                             (flycheck-sanitize-errors (flycheck-increment-error-columns errors)))
                                           :error-patterns
                                           ((error line-start (file-name) ":" line ":" column ":"
                                                   (or "E" "F") ":"
                                                   (id (one-or-more (not (any ":")))) ":"
                                                   (message) line-end)
                                            (warning line-start (file-name) ":" line ":" column ":"
                                                     (or "W" "R") ":"
                                                     (id (one-or-more (not (any ":")))) ":"
                                                     (message) line-end)
                                            (info line-start (file-name) ":" line ":" column ":"
                                                  "C:" (id (one-or-more (not (any ":")))) ":"
                                                  (message) line-end))
                                           :modes python-mode)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;; Diğerleri;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key (kbd "C-ö") 'move-text-up)
(global-set-key (kbd "C-ç") 'move-text-down)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Liberation Mono" :foundry "1ASC" :slant normal :weight regular :width normal))))
 '(whitespace-line ((t (:background "gray23" :foreground "deep sky blue"))))
 '(whitespace-trailing ((t (:background "gray23" :foreground "yellow" :weight bold)))))

;; (add-to-list 'default-frame-alist '(font .  "Liberation Mono-24" ))
;; (set-face-attribute 'default t :font  "Liberation Mono-24" )

;; (setq wl-copy-process nil)
;; (defun wl-copy (text)
;;   (setq wl-copy-process (make-process :name "wl-copy"
;;                                       :buffer nil
;;                                       :command '("wl-copy" "-f" "-n")
;;                                       :connection-type 'pipe))
;;   (process-send-string wl-copy-process text)
;;   (process-send-eof wl-copy-process))
;; (defun wl-paste ()
;;   (if (and wl-copy-process (process-live-p wl-copy-process))
;;       nil ; should return nil if we're the current paste owner
;;     (shell-command-to-string "wl-paste -n | tr -d \r")))
;; (setq interprogram-cut-function 'wl-copy)
;; (setq interprogram-paste-function 'wl-paste)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'cl)
(require 'dash)

(defun todo-to-int (todo)
    (first (-non-nil
            (mapcar (lambda (keywords)
                      (let ((todo-seq
                             (-map (lambda (x) (first (split-string  x "(")))
                                   (rest keywords))))
                        (cl-position-if (lambda (x) (string= x todo)) todo-seq)))
                    org-todo-keywords))))

(defun my/org-sort-key ()
  (let* ((todo-max (apply #'max (mapcar #'length org-todo-keywords)))
         (todo (org-entry-get (point) "SIRADA"))
         (todo-int (if todo (todo-to-int todo) todo-max))
         (priority (org-entry-get (point) "PRIORITY"))
         (priority-int (if priority (string-to-char priority) org-default-priority)))
    (format "%03d %03d" todo-int priority-int)
    ))

(defun my/org-sort-entries ()
  (interactive)
  (org-sort-entries nil ?f #'my/org-sort-key))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun set-exec-path-from-shell-PATH ()
  (let ((path-from-shell (replace-regexp-in-string
                          "[ \t\n]*$"
                          ""
                          (shell-command-to-string "$SHELL --login -i -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq eshell-path-env path-from-shell) ; for eshell users
    (setq exec-path (split-string path-from-shell path-separator))))

(when window-system (set-exec-path-from-shell-PATH))
