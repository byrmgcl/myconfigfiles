#!/bin/bash

# Stackoverflow reputation score and Emoji on change :)
# BitBar plugin
#
# <bitbar.title>Stackoverflow Reputation Score</bitbar.title>
# <bitbar.author>Bruce Steedman</bitbar.author>
# <bitbar.author.github>MatzFan</bitbar.author.github>
# <bitbar.image>https://i.imgur.com/0XHs8R0.png</bitbar.image>
# <bitbar.version>1.0</bitbar.version>
#
# by Bruce Steedman
#
# Shows current reputation and 👍 / 👎 on change
# 5 minute refresh is just under the 300 calls a day no-key api limit

# id for user from url - e.g. https://stackoverflow.com/users/4114896/matzfan
SO_ID="byrmgcl" # CHANGE THIS VALUE to your's (or watch Mr Skeet reach 7 figures!)

URI="https://www.codewars.com/api/v1/users/$SO_ID" # api
SO_ICON=""

# file used to persist old score. set score to 0 if file doesn't exist
if [ ! -f "/tmp/codewars.dat" ] ; then
  OLDREP=0
else
  OLDREP=$(cat /tmp/codewars.dat) # read value from file
fi
# get reputation from api
NEWREP=$(curl -s --compressed "$URI" | egrep -o '"score":*([0-9])+' | sed 's/"score"://')
read -ra NEWREP <<< $NEWREP

if [ -z "$OLDREP" ] || [ -z "$NEWREP" ] ; then
  echo "CW ? | color=orange image=$SO_ICON"
elif [ "$NEWREP" -gt "$OLDREP" ] ; then
  echo "CW +$((NEWREP - OLDREP)) | color=green image=$SO_ICON"
elif [ "$OLDREP" -gt "$NEWREP" ] ; then
  echo "CW $((NEWREP - OLDREP)) | color=red image=$SO_ICON"
else
  echo "CW $NEWREP | image=$SO_ICON" # output score
fi

echo "$NEWREP" > /tmp/codewars.dat # write new score to file
