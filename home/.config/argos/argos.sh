#!/usr/bin/env bash

#CherryTree
echo "| iconName=debian-swirl"
echo "---"
echo "<span color='#9BF' weight='bold'><small><tt>CherryTree</tt></small></span> | size=16"

echo "-- Bugünün Notları | href='file:///home/byrmgcl/Files/notlar/cherrytree/Bugunun_Notlari.ctb'"
echo "-- Notlar | href='file:///home/byrmgcl/Files/notlar/cherrytree/Notlar.ctb'"

echo "-- Geliştirme | href='file:///home/byrmgcl/Files/notlar/cherrytree/Gelistirme.ctb'"
echo "-- Kitaplardan Notlar | href='file:///home/byrmgcl/Files/notlar/cherrytree/Kitaplardan_Notlar.ctb'"


# Redshift
echo "<span color='#9BF' weight='bold'><small><tt>Redshift</tt></small></span> | size=16"
echo "-- 5 dakikalığına kapat | bash='red 5' terminal=false"
echo "-- 15 dakikalığına kapat | bash='red 15' terminal=false"
echo "-- 30 dakikalığına kapat | bash='red 30' terminal=false"
echo "-- 1 saatliğine kapat | bash='red 60' terminal=false"
echo "-- 2 saatliğine kapat | bash='red 120' terminal=false"
